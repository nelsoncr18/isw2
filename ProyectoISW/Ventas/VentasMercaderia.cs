﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoISW.Ventas
{
    public partial class VentasMercaderia : Form
    {
        public VentasMercaderia()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=proyectoisw;";
            // Seleccionar todo
            string query = "SELECT v.cantidad, v.fecha, v.impuesto, v.total FROM `ventas` v";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();


                // Si se encontraron datos
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int n = datagrid.Rows.Add();

                        datagrid.Rows[n].Cells[0].Value = (reader.GetString(0));
                        datagrid.Rows[n].Cells[1].Value = (reader.GetString(1));
                        datagrid.Rows[n].Cells[2].Value = (reader.GetString(2));
                        datagrid.Rows[n].Cells[3].Value = (reader.GetString(3));
                    }
                }
                else
                {
                    Console.WriteLine("No se encontro nada");
                }

                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
    }
}
