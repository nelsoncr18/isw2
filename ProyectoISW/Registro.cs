﻿using Habanero.Util;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoISW
{
    public partial class Registro : Form
    {
        public Registro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=proyectoisw;";
            string query = "INSERT INTO `usuario`(`nombre`, `apellido`, `telefono`, `direccion`, `tipo_usuario`, `contraseña`, `nombre_usuario`) VALUES ('" + txtNombre.Text + "', '" + txtApellido.Text + "', '" + txtTelefono.Text + "', '" + txtDireccion.Text + "','Empleado', '" + txtContraseña.Text + "', '" + txtNombreUsuario.Text + "' )";

            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;

            if (txtNombre.Text.Contains("") && txtApellido.Text.Contains("") && txtDireccion.Text.Contains("") && txtContraseña.Text.Contains("") && txtNombreUsuario.Text.Contains("") && txtTelefono.Text.Contains(""))
            {
                MessageBox.Show("No puede dejar campos en blanco");
            }
            else
            {
                try
                {
                    databaseConnection.Open();
                    MySqlDataReader myReader = commandDatabase.ExecuteReader();

                    MessageBox.Show("Usuario registrado Correctamente");

                    databaseConnection.Close();
                }
                catch (Exception ex)
                {
                    // Mostrar cualquier error
                    MessageBox.Show("No se pudo registrar este usuario", ex.Message);

                }
                this.Close();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
