﻿using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;

namespace ProyectoISW.Inventario
{
    public partial class MostrarInventario : Form
    {
        public MostrarInventario()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=proyectoisw;";
            // Seleccionar todo
            string query = "SELECT m.marca, m.categoria, m.precio,m.cantidad FROM `mercaderia` m";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();


                // Si se encontraron datos
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int n = datagrid.Rows.Add();

                        datagrid.Rows[n].Cells[0].Value = (reader.GetString(0));
                        datagrid.Rows[n].Cells[1].Value = (reader.GetString(1));
                        datagrid.Rows[n].Cells[2].Value = (reader.GetString(2));
                        datagrid.Rows[n].Cells[3].Value = (reader.GetString(3));
                    }
                }
                else
                {
                    Console.WriteLine("No se encontro nada");
                }

                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
    }
}
