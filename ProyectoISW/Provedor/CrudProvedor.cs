﻿using MySql.Data.MySqlClient;
using MySqlX.XDevAPI;
using Org.BouncyCastle.Asn1.Cmp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoISW.Provedor
{
    public partial class CrudProvedor : Form
    {

        int Id;
        Boolean editar;
        public CrudProvedor()
        {
            InitializeComponent();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregarProvedor_Click(object sender, EventArgs e)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=proyectoisw;";

            if (editar)
            {
                string query = "UPDATE `provedor` SET `nombre`= '" + txtNombre.Text + "' ,`cedula`= '" + txtCedula.Text + "' ,`direccion`= '" + txtDireccion.Text + "' ,`telefono`= '" + txtTelefono.Text + "' WHERE id_provedor =  '" + Id + "'";
                MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.CommandTimeout = 60;
                databaseConnection.Open();
                MySqlDataReader myReader = commandDatabase.ExecuteReader();
                MessageBox.Show("Provedor editado correctamente");

                databaseConnection.Close();
                editar = false;
            }
            else
            {
                try
                {
                string query = "INSERT INTO `provedor`(`id_provedor`,`nombre`, `cedula`, `direccion`, `telefono`) VALUES (NULL ,'" + txtNombre.Text + "', '" + txtCedula.Text + "', '" + txtDireccion.Text + "', '" + txtTelefono.Text + "' )";
                    MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                    MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
                    commandDatabase.CommandTimeout = 60;
                    databaseConnection.Open();
                    MySqlDataReader myReader = commandDatabase.ExecuteReader();

                    MessageBox.Show("Campo insertado satisfactoriamente");
                    Actualizar();
                    databaseConnection.Close();
                }
                catch (Exception ex)
                {
                    // Mostrar cualquier error
                    MessageBox.Show(ex.Message);

                }
                //this.Close();
            }
        }


        public void Actualizar()
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=proyectoisw;";
            // Seleccionar todo
            string query = "SELECT p.id_provedor,p.nombre, p.cedula, p.direccion, p.telefono FROM provedor p";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();


                // Si se encontraron datos
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int n = datagrid.Rows.Add();

                        datagrid.Rows[n].Cells[0].Value = (reader.GetString(0));
                        datagrid.Rows[n].Cells[1].Value = (reader.GetString(1));
                        datagrid.Rows[n].Cells[2].Value = (reader.GetString(2));
                        datagrid.Rows[n].Cells[3].Value = (reader.GetString(3));
                        datagrid.Rows[n].Cells[4].Value = (reader.GetString(4));
                    }
                }
                else
                {
                    Console.WriteLine("No se encontro nada");
                }

                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnEditarProvedor_Click(object sender, EventArgs e)
        {
            editar = true;
            Id = int.Parse(this.datagrid.CurrentRow.Cells[0].Value.ToString());
            txtNombre.Text = this.datagrid.CurrentRow.Cells[1].Value.ToString();
            txtCedula.Text = this.datagrid.CurrentRow.Cells[2].Value.ToString();
            txtDireccion.Text = this.datagrid.CurrentRow.Cells[3].Value.ToString();
            txtTelefono.Text = this.datagrid.CurrentRow.Cells[4].Value.ToString();
        }

        private void btnEliminarProvedor_Click(object sender, EventArgs e)
        {
            Id = int.Parse(this.datagrid.CurrentRow.Cells[0].Value.ToString());
            var resultado = MessageBox.Show("¿Desea eliminar ese registro?", "Confirme por favor", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            string query = "DELETE FROM `provedor` WHERE id_provedor = '" + Id + "'";
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=proyectoisw;";

            if (resultado == DialogResult.Yes)
            {
                MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.CommandTimeout = 60;
                MySqlDataReader reader;
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                MessageBox.Show("Dato eliminado");
            }
        }
    }
}
