﻿using MySql.Data.MySqlClient;
using ProyectoISW.Admin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoISW
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            Registro frmRegistro = new Registro();
            frmRegistro.ShowDialog();
        }

        public void limpiar()
        {
            txtIniciarSesion.Clear();
            txtPassword.Clear();
        }

        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=proyectoisw;";
            //// Seleccionar todo
            string query = "SELECT `nombre`, `contraseña` FROM `usuario` u";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();

                    // Si se encontraron datos
                    if (reader.HasRows)
                    {
                         while (reader.Read())
                        {

                            string nombre = reader.GetString(0);
                            string contraseña = (reader.GetString(1));

                            if (txtIniciarSesion.Text.ToString() == nombre && txtPassword.Text.ToString() == contraseña)
                            {
                                MessageBox.Show("Bienvendo: " + nombre);
                                Admin.Admin frmAdmin = new Admin.Admin();
                                frmAdmin.ShowDialog();
                                limpiar();
                            }
                          }
                        }
                        else
                        {
                            Console.WriteLine("No se encontro nada");
                        }
                    databaseConnection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al iniciar sesion verifique sus credenciales",ex.Message);
                }

        }
    }
}
