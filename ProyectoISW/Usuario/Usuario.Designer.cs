﻿namespace ProyectoISW.Usuario
{
    partial class Usuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnProbando = new System.Windows.Forms.Button();
            this.database = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.database)).BeginInit();
            this.SuspendLayout();
            // 
            // btnProbando
            // 
            this.btnProbando.Location = new System.Drawing.Point(216, 229);
            this.btnProbando.Name = "btnProbando";
            this.btnProbando.Size = new System.Drawing.Size(103, 44);
            this.btnProbando.TabIndex = 0;
            this.btnProbando.Text = "Probando ";
            this.btnProbando.UseVisualStyleBackColor = true;
            // 
            // database
            // 
            this.database.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.database.Location = new System.Drawing.Point(12, 12);
            this.database.Name = "database";
            this.database.Size = new System.Drawing.Size(686, 193);
            this.database.TabIndex = 1;
            // 
            // Usuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 450);
            this.Controls.Add(this.database);
            this.Controls.Add(this.btnProbando);
            this.Name = "Usuario";
            this.Text = "Usuario";
            ((System.ComponentModel.ISupportInitialize)(this.database)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnProbando;
        private System.Windows.Forms.DataGridView database;
    }
}