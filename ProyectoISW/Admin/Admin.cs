﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoISW.Admin
{
    public partial class Admin : Form
    {
        public Admin()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMercaderia_Click(object sender, EventArgs e)
        {
            CrudZapatos frmCrudZapatos = new CrudZapatos();
            frmCrudZapatos.ShowDialog();
        }

        private void btnProvedor_Click(object sender, EventArgs e)
        {
            Provedor.CrudProvedor frmCrudProvedor = new Provedor.CrudProvedor();
            frmCrudProvedor.ShowDialog();
        }

        private void btnVenta_Click(object sender, EventArgs e)
        {
            Ventas.VentasMercaderia frmVentasMercaderia = new Ventas.VentasMercaderia();
            frmVentasMercaderia.ShowDialog();
        }

        private void btnMostrarInventario_Click(object sender, EventArgs e)
        {
            Inventario.MostrarInventario frmMostrarinventario = new Inventario.MostrarInventario();
            frmMostrarinventario.ShowDialog();
        }

        private void btnMostrarVenta_Click(object sender, EventArgs e)
        {
            Ventas.VentasMercaderia frmVentas = new Ventas.VentasMercaderia();
            frmVentas.ShowDialog();
        }
    }
}
