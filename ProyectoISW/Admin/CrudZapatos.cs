﻿using Habanero.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoISW.Admin
{
    public partial class CrudZapatos : Form
    {
        int Id;
        Boolean editar;
        public CrudZapatos()
        {
            InitializeComponent();
            editar = false;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregarZapatos_Click(object sender, EventArgs e)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=proyectoisw;";

            if (editar)
            {
                string query = "UPDATE `mercaderia` SET `marca`= '" + txtMarca.Text + "', `categoria`= '" + txtCategoria.Text + "',`precio`= '" + txtPrecio.Text + "',`cantidad`=  '" + txtCantidad.Text + "' WHERE `id_mercaderia` = '" + Id + "'";

                MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.CommandTimeout = 60;
                databaseConnection.Open();
                MySqlDataReader myReader = commandDatabase.ExecuteReader();
                MessageBox.Show("Mercaderia editada correctamente");

                databaseConnection.Close();
                editar = false;
            }
            else
            {
                try
                {
                    string query = "INSERT INTO `mercaderia`(`id_mercaderia`, `marca`, `categoria`, `id_provedor`, `precio`, `cantidad`) VALUES (NULL, '" + txtMarca.Text + "', '" + txtCategoria.Text + "', '" + 1 + "', '" + txtPrecio.Text + "', '" + txtCantidad.Text + "' )";

                    MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                    MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
                    commandDatabase.CommandTimeout = 60;
                    databaseConnection.Open();
                    MySqlDataReader myReader = commandDatabase.ExecuteReader();

                    MessageBox.Show("Mercaderia insertada satisfactoriamente");

                    databaseConnection.Close();
                }
                catch (Exception ex)
                {
                    // Mostrar cualquier error
                    MessageBox.Show(ex.Message);

                }
            }
        }

        public void actualizar()
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=proyectoisw;";
            // Seleccionar todo
            string query = "SELECT DISTINCT m.id_mercaderia,m.marca, m.categoria, m.precio,m.cantidad FROM `mercaderia` m";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();


                // Si se encontraron datos
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int n = datagrid.Rows.Add();

                        datagrid.Rows[n].Cells[0].Value = (reader.GetString(0));
                        datagrid.Rows[n].Cells[1].Value = (reader.GetString(1));
                        datagrid.Rows[n].Cells[2].Value = (reader.GetString(2));
                        datagrid.Rows[n].Cells[3].Value = (reader.GetString(3));
                        datagrid.Rows[n].Cells[4].Value = (reader.GetString(4));
                    }
                }
                else
                {
                    Console.WriteLine("No se encontro nada");
                }

                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            actualizar();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            editar = true;
            Id = int.Parse(this.datagrid.CurrentRow.Cells[0].Value.ToString());
            txtMarca.Text = this.datagrid.CurrentRow.Cells[1].Value.ToString();
            txtCategoria.Text = this.datagrid.CurrentRow.Cells[2].Value.ToString();
            txtPrecio.Text = this.datagrid.CurrentRow.Cells[3].Value.ToString();
            txtCantidad.Text = this.datagrid.CurrentRow.Cells[4].Value.ToString();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Id = int.Parse(this.datagrid.CurrentRow.Cells[0].Value.ToString());
            var resultado = MessageBox.Show("¿Desea eliminar ese registro?", "Confirme por favor", MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            string query = "DELETE FROM `mercaderia` WHERE id_mercaderia = '" + Id + "'";
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=proyectoisw;";

            if (resultado == DialogResult.Yes)
            {
                MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.CommandTimeout = 60;
                MySqlDataReader reader;
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                MessageBox.Show("Dato eliminado");
            }
        }
    }
}
