﻿namespace ProyectoISW.Admin
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMostrarVenta = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnMostrarInventario = new System.Windows.Forms.Button();
            this.btnVenta = new System.Windows.Forms.Button();
            this.btnProvedor = new System.Windows.Forms.Button();
            this.btnMercaderia = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnMostrarVenta
            // 
            this.btnMostrarVenta.Location = new System.Drawing.Point(468, 12);
            this.btnMostrarVenta.Name = "btnMostrarVenta";
            this.btnMostrarVenta.Size = new System.Drawing.Size(108, 46);
            this.btnMostrarVenta.TabIndex = 11;
            this.btnMostrarVenta.Text = "Mostrar ventas";
            this.btnMostrarVenta.UseVisualStyleBackColor = true;
            this.btnMostrarVenta.Click += new System.EventHandler(this.btnMostrarVenta_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(582, 12);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(108, 46);
            this.btnSalir.TabIndex = 10;
            this.btnSalir.Text = "Cerrar sesion";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnMostrarInventario
            // 
            this.btnMostrarInventario.Location = new System.Drawing.Point(354, 12);
            this.btnMostrarInventario.Name = "btnMostrarInventario";
            this.btnMostrarInventario.Size = new System.Drawing.Size(108, 46);
            this.btnMostrarInventario.TabIndex = 9;
            this.btnMostrarInventario.Text = "Mostrar inventario";
            this.btnMostrarInventario.UseVisualStyleBackColor = true;
            this.btnMostrarInventario.Click += new System.EventHandler(this.btnMostrarInventario_Click);
            // 
            // btnVenta
            // 
            this.btnVenta.Location = new System.Drawing.Point(240, 12);
            this.btnVenta.Name = "btnVenta";
            this.btnVenta.Size = new System.Drawing.Size(108, 46);
            this.btnVenta.TabIndex = 8;
            this.btnVenta.Text = "Venta de mercaderia";
            this.btnVenta.UseVisualStyleBackColor = true;
            this.btnVenta.Click += new System.EventHandler(this.btnVenta_Click);
            // 
            // btnProvedor
            // 
            this.btnProvedor.Location = new System.Drawing.Point(126, 12);
            this.btnProvedor.Name = "btnProvedor";
            this.btnProvedor.Size = new System.Drawing.Size(108, 46);
            this.btnProvedor.TabIndex = 7;
            this.btnProvedor.Text = "Administrar provedor";
            this.btnProvedor.UseVisualStyleBackColor = true;
            this.btnProvedor.Click += new System.EventHandler(this.btnProvedor_Click);
            // 
            // btnMercaderia
            // 
            this.btnMercaderia.Location = new System.Drawing.Point(12, 12);
            this.btnMercaderia.Name = "btnMercaderia";
            this.btnMercaderia.Size = new System.Drawing.Size(108, 46);
            this.btnMercaderia.TabIndex = 6;
            this.btnMercaderia.Text = "Administrar mercaderia";
            this.btnMercaderia.UseVisualStyleBackColor = true;
            this.btnMercaderia.Click += new System.EventHandler(this.btnMercaderia_Click);
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 451);
            this.Controls.Add(this.btnMostrarVenta);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnMostrarInventario);
            this.Controls.Add(this.btnVenta);
            this.Controls.Add(this.btnProvedor);
            this.Controls.Add(this.btnMercaderia);
            this.Name = "Admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMostrarVenta;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnMostrarInventario;
        private System.Windows.Forms.Button btnVenta;
        private System.Windows.Forms.Button btnProvedor;
        private System.Windows.Forms.Button btnMercaderia;
    }
}